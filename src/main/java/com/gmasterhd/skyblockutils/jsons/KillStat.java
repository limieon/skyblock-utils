package com.gmasterhd.skyblockutils.jsons;

public class KillStat {
	public String entity;
	public int value;
	
	public KillStat(String entity, int value) {
		this.entity = entity;
		this.value = value;
	}
	
	public void setValue(int value) {
		this.value = value;
	}
	public void addValue() {
		++this.value;
	}
	public void subtractValue() {
		--this.value;
	}
}
