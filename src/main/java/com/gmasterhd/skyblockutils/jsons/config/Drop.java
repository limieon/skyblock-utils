package com.gmasterhd.skyblockutils.jsons.config;

import com.gmasterhd.skyblockutils.enums.Rarity;

public class Drop {
	public String name;
	public String chatTrigger;
	public Rarity rarity;
	
	public Drop(String name, String chatTrigger, Rarity rarity) {
		this.name = name;
		this.chatTrigger = chatTrigger;
		this.rarity = rarity;
	}
	public Drop(String name, Rarity rarity) {
		this.name = name;
		this.chatTrigger = name;
		this.rarity = rarity;
	}
}
