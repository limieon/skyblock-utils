package com.gmasterhd.skyblockutils.jsons;

import java.util.ArrayList;
import java.util.List;

public class PersistentValues {
	public String selectedEntity = "";
	public String apiKey = "";
	public boolean skillInfoVisible = false;
	public List<KillStat> KillStats = new ArrayList();
	public PersistentSkill Combat = new PersistentSkill("Combat");
	public PersistentSkill Mining = new PersistentSkill("Mining");
	public PersistentSkill Foraging = new PersistentSkill("Foraging");
	public PersistentSkill Enchanting = new PersistentSkill("Enchanting");
	public PersistentSkill Fishing = new PersistentSkill("Fishing");
	public PersistentSkill Taming = new PersistentSkill("Taming");
	public PersistentSkill Farming = new PersistentSkill("Farming");
	public PersistentSkill Alchemy = new PersistentSkill("Alchemy");
	public List<Slayer> Slayers = new ArrayList();
}
