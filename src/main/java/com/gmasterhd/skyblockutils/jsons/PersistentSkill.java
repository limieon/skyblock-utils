package com.gmasterhd.skyblockutils.jsons;

public class PersistentSkill {
	public String skillName;
	public int level;
	public double xpGained;
	public double neededXP;
	public double xpLeft;
	public double actionsLeft;
	public double currentXP;
	public double lastXP;
	public float xphour;
	public double totalXP;
	
	public PersistentSkill(String skillName, int level, double neededXP, double xpLeft, double actionsLeft) {
		this.skillName = skillName;
		this.xpGained = 0;
		this.neededXP = neededXP;
		this.xpLeft = xpLeft;
		this.actionsLeft = actionsLeft;
		this.currentXP = 0;
		this.level = level;
		this.xphour = 0;
	}
	public PersistentSkill(String name) {
		this.skillName = name;
		this.xpGained = 0;
		this.neededXP = 0;
		this.xpLeft = 0;
		this.actionsLeft = 0;
		this.currentXP = 0;
		this.level = 0;
		this.xphour = 0;
	}
}
