package com.gmasterhd.skyblockutils;

import com.gmasterhd.skyblockutils.commands.*;
import com.gmasterhd.skyblockutils.enums.Rarity;
import com.gmasterhd.skyblockutils.guis.SkillXPDisplay;
import com.gmasterhd.skyblockutils.jsons.PersistentSkill;
import com.gmasterhd.skyblockutils.jsons.PersistentValues;
import com.gmasterhd.skyblockutils.jsons.config.Config;
import com.gmasterhd.skyblockutils.jsons.config.Drop;
import com.gmasterhd.skyblockutils.jsons.config.Slayer;
import com.gmasterhd.skyblockutils.listeners.PlayerListener;
import com.gmasterhd.skyblockutils.listeners.RenderListener;
import com.gmasterhd.skyblockutils.listeners.SlayerListener;
import com.gmasterhd.skyblockutils.utils.PersistentFile;

import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.*;

@Mod(modid = SkyBlockUtils.MODID, version = SkyBlockUtils.VERSION, clientSideOnly = true, acceptedMinecraftVersions = "[1.8.9]")
public class SkyBlockUtils {
	public static final String MODID = "skyblockutils";
	public static final String VERSION = "0.2.0";
	
	public static SkillXPDisplay skillXPDisplay;
	
	public static PersistentValues saves = new PersistentValues();
	public static Config config = new Config();
	
	public static PersistentFile<PersistentValues> file;
	public static PersistentFile<Config> configFile;
	
	@Mod.Instance
	public static SkyBlockUtils instance = new SkyBlockUtils();
	
	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent e) {
		saves = new PersistentValues();
		saves.Slayers.add(new com.gmasterhd.skyblockutils.jsons.Slayer("Revenant Horror"));
		saves.Slayers.add(new com.gmasterhd.skyblockutils.jsons.Slayer("Tarantula Broodfather"));
		saves.Slayers.add(new com.gmasterhd.skyblockutils.jsons.Slayer("Sven Packmaster"));
		saves.Alchemy = new PersistentSkill("Alchemy");
		saves.Combat = new PersistentSkill("Combat");
		saves.Enchanting = new PersistentSkill("Enchanting");
		saves.Farming = new PersistentSkill("Farming");
		saves.Fishing = new PersistentSkill("Fishing");
		saves.Foraging = new PersistentSkill("Foraging");
		saves.Mining = new PersistentSkill("Mining");
		saves.Taming = new PersistentSkill("Taming");
		saves.selectedEntity = "Enderman";
		
		file = new PersistentFile<PersistentValues>(e.getModConfigurationDirectory().getPath() + "/skyblockutils/saves.json", saves);
		saves = file.load(PersistentValues.class);
		file.save(saves);
		
		Slayer zombie = new Slayer("Revenant Horror", "Zombie", "evenant Horr");
		zombie.Drops.add(new Drop("Foul Flesh", Rarity.RARE));
		zombie.Drops.add(new Drop("Undead Catalyst", Rarity.RARE));
		zombie.Drops.add(new Drop("Pestilence Rune", Rarity.RARE));
		zombie.Drops.add(new Drop("Smite VI", "Enchanted Book", Rarity.RARE));
		zombie.Drops.add(new Drop("Beheaded Horror", Rarity.RARE));
		zombie.Drops.add(new Drop("Revenant Catalyst", Rarity.RARE));
		zombie.Drops.add(new Drop("Snake Rune", Rarity.RARE));
		zombie.Drops.add(new Drop("Scythe Blade", Rarity.RARE));
		
		Slayer spider = new Slayer("Tarantula Broodfather", "Spider", "rantula Broodfat");
		spider.Drops.add(new Drop("Toxic Arrow Poison", Rarity.UNCOMMON));
		spider.Drops.add(new Drop("Bite Rune", Rarity.EPIC));
		spider.Drops.add(new Drop("Spider Catalyst", Rarity.RARE));
		spider.Drops.add(new Drop("Bane of Arthopods VI", "Enchanted Book", Rarity.RARE));
		spider.Drops.add(new Drop("Fly Swatter", Rarity.EPIC));
		spider.Drops.add(new Drop("Tarantula Talisman", Rarity.EPIC));
		spider.Drops.add(new Drop("Disgested Mosquito", Rarity.LEGENDARY));
		
		Slayer wolf = new Slayer("Sven Packmaster", "Wolf", "ven Packmast");
		wolf.Drops.add(new Drop("Hamster Wheel", Rarity.RARE));
		wolf.Drops.add(new Drop("Spirit Rune", Rarity.RARE));
		wolf.Drops.add(new Drop("Critical VI", "Enchanted Book", Rarity.RARE));
		wolf.Drops.add(new Drop("Red Claw Egg", Rarity.EPIC));
		wolf.Drops.add(new Drop("Grizzly Bait", Rarity.RARE));
		wolf.Drops.add(new Drop("Couture Rune", Rarity.LEGENDARY));
		wolf.Drops.add(new Drop("Overflux Capacitor", Rarity.LEGENDARY));
		
		config.Slayers.add(zombie);
		config.Slayers.add(spider);
		config.Slayers.add(wolf);
		
		configFile = new PersistentFile<Config>(e.getModConfigurationDirectory().getPath() + "/" + MODID + "/config.json", config);
		config = configFile.load(Config.class);
		configFile.save(config);
		
		skillXPDisplay = new SkillXPDisplay();
		
		MinecraftForge.EVENT_BUS.register(new PlayerListener());
		MinecraftForge.EVENT_BUS.register(new RenderListener());
		MinecraftForge.EVENT_BUS.register(new SlayerListener());
		
		RenderListener.changeEntity(saves.selectedEntity);
	}
	@Mod.EventHandler
	public void init(FMLInitializationEvent e) {
	}
	@Mod.EventHandler
	public void postInit(FMLPostInitializationEvent e) {
		ClientCommandHandler.instance.registerCommand(new CommandSkills());
		ClientCommandHandler.instance.registerCommand(new CommandAPIKey());
		ClientCommandHandler.instance.registerCommand(new CommandSlayer());
		ClientCommandHandler.instance.registerCommand(new CommandKills());
		ClientCommandHandler.instance.registerCommand(new CommandAPIImport());
		ClientCommandHandler.instance.registerCommand(new CommandSkillInfo());
	}
	
	@Mod.EventHandler
	public void fmlLiveCycle(FMLServerStoppedEvent e) {
		file.save(saves);
	}
}
