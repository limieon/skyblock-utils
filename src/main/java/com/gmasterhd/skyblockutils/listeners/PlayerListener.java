package com.gmasterhd.skyblockutils.listeners;

import akka.actor.Kill;
import com.gmasterhd.skyblockutils.SkyBlockUtils;
import com.gmasterhd.skyblockutils.jsons.KillStat;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.item.EntityArmorStand;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.ChatComponentText;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class PlayerListener {
	private Set<UUID> countedEndermen = new HashSet();
	
	@SubscribeEvent
	public void onWorldJoin(EntityJoinWorldEvent e) {
		if(e.entity == (Minecraft.getMinecraft().thePlayer)) {
			countedEndermen.clear();
		}
	}
	
	@SubscribeEvent
	public void onAttack(AttackEntityEvent e) {
		List<EntityArmorStand> stands = (Minecraft.getMinecraft()).theWorld.getEntitiesWithinAABB(EntityArmorStand.class, new AxisAlignedBB(e.target.posX - 1.0D, e.target.posY, e.target.posZ - 1.0D, e.target.posX + 1.0D, e.target.posY + 5.0D, e.target.posZ + 1.0D));
		if (stands.isEmpty()) {
			return;
		}
		EntityArmorStand armorStand = stands.get(0);
		if (armorStand.hasCustomName() && armorStand.getCustomNameTag().contains(SkyBlockUtils.saves.selectedEntity)) {
			countedEndermen.add(e.target.getUniqueID());
		}
	}
	
	@SubscribeEvent
	public void onDeath(LivingDeathEvent e) {
		if (/*e.entity instanceof net.minecraft.entity.monster.EntityEnderman && */this.countedEndermen.remove(e.entity.getUniqueID())) {
			boolean found = false;
			for(int x = 0; x < SkyBlockUtils.saves.KillStats.size(); ++x) {
				KillStat ks = SkyBlockUtils.saves.KillStats.get(x);
				if(ks.entity.equals(SkyBlockUtils.saves.selectedEntity)) {
					found = true;
					SkyBlockUtils.saves.KillStats.set(x, new KillStat(ks.entity, ++ks.value));
					Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText("SkyBlock Utils > " + SkyBlockUtils.saves.selectedEntity + " Kills: " + ks.value));
				}
			}

			if(!found) {
				SkyBlockUtils.saves.KillStats.add(new KillStat(SkyBlockUtils.saves.selectedEntity, 1));
				Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText("SkyBlock Utils > " + SkyBlockUtils.saves.selectedEntity + " Kills: " + 1));
			}
			
			SkyBlockUtils.file.save(SkyBlockUtils.saves);
		}
	}
}
