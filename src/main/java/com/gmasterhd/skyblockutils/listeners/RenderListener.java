package com.gmasterhd.skyblockutils.listeners;
		
import com.gmasterhd.skyblockutils.SkyBlockUtils;
import com.gmasterhd.skyblockutils.enums.Skill;
import com.gmasterhd.skyblockutils.jsons.KillStat;
import com.gmasterhd.skyblockutils.utils.*;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.ClientChatReceivedEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import org.lwjgl.input.Mouse;
import scala.collection.parallel.ParIterableLike;

import java.io.Serializable;
import java.util.ArrayList;

public class RenderListener implements Serializable {
	public static double lastXP = -1;
	public static Skill selectedSkill = Skill.MINING;
	
	private static float zLevel = 0.0F;
	
	private static String entityTexture = "";
	
	private static int x_skillXPDisplay, y_skillXPDisplay;
	public static int mouseX = 0, mouseY = 0;
	
	public static void changeEntity(String entity) {
		entityTexture = EntityHandler.getEntityImage(entity);
	}
	
	private double combat_currentTime = 1;
	private double combat_lastTime = 1;
	private double combat_currentXP = 1;
	private double combat_lastXP = 1;
	
	private double alchemy_currentTime = 1;
	private double alchemy_lastTime = 1;
	private double alchemy_currentXP = 1;
	private double alchemy_lastXP = 1;
	
	private double enchanting_currentTime = 1;
	private double enchanting_lastTime = 1;
	private double enchanting_currentXP = 1;
	private double enchanting_lastXP = 1;
	
	private double mining_currentTime = 1;
	private double mining_lastTime = 1;
	private double mining_currentXP = 1;
	private double mining_lastXP = 1;
	
	private double foraging_currentTime = 1;
	private double foraging_lastTime = 1;
	private double foraging_currentXP = 1;
	private double foraging_lastXP = 1;
	
	private double farming_currentTime = 1;
	private double farming_lastTime = 1;
	private double farming_currentXP = 1;
	private double farming_lastXP = 1;
	
	private double fishing_currentTime = 1;
	private double fishing_lastTime = 1;
	private double fishing_currentXP = 1;
	private double fishing_lastXP = 1;
	
	private double taming_currentTime = 1;
	private double taming_lastTime = 1;
	private double taming_currentXP = 1;
	private double taming_lastXP = 1;
	
	@SubscribeEvent
	public void onRenderGameOverlay(RenderGameOverlayEvent e) {
		if(!e.isCancelable()) {
			Minecraft mc = Minecraft.getMinecraft();
			double width = 16;
			
			FontRenderer fr = Minecraft.getMinecraft().fontRendererObj;
			
			if(mouseX >= x_skillXPDisplay && mouseX <= x_skillXPDisplay + 16 && mouseY >= y_skillXPDisplay && mouseY <= y_skillXPDisplay + 16 && !SkyBlockUtils.saves.skillInfoVisible) {
				ArrayList<String> tooltip = new ArrayList<String>();
				if(selectedSkill == Skill.ALCHEMY) {
					tooltip.add(EnumChatFormatting.BOLD + "Alchemy " + SkyBlockUtils.saves.Alchemy.level);
					tooltip.add(EnumChatFormatting.GOLD + "XP: " + SkyBlockUtils.saves.Alchemy.currentXP + " / " + SkyBlockUtils.saves.Alchemy.neededXP);
					tooltip.add(EnumChatFormatting.GOLD + "XP/hr: " + SkyBlockUtils.saves.Alchemy.xphour);
					tooltip.add(EnumChatFormatting.GOLD + "Actions left: " + (int) SkyBlockUtils.saves.Alchemy.actionsLeft);
				} else if(selectedSkill == Skill.COMBAT) {
					tooltip.add(EnumChatFormatting.BOLD + "Combat " + SkyBlockUtils.saves.Combat.level);
					tooltip.add(EnumChatFormatting.GOLD + "XP: " + SkyBlockUtils.saves.Combat.currentXP + " / " + SkyBlockUtils.saves.Combat.neededXP);
					tooltip.add(EnumChatFormatting.GOLD + "XP/hr: " + SkyBlockUtils.saves.Combat.xphour);
					tooltip.add(EnumChatFormatting.GOLD + "Actions left: " + (int) SkyBlockUtils.saves.Combat.actionsLeft);
				} else if(selectedSkill == Skill.ENCHANTING) {
					tooltip.add(EnumChatFormatting.BOLD + "Enchanting " + SkyBlockUtils.saves.Enchanting.level);
					tooltip.add(EnumChatFormatting.GOLD + "XP: " + SkyBlockUtils.saves.Enchanting.currentXP + " / " + SkyBlockUtils.saves.Enchanting.neededXP);
					tooltip.add(EnumChatFormatting.GOLD + "XP/hr: " + + SkyBlockUtils.saves.Enchanting.xphour);
					tooltip.add(EnumChatFormatting.GOLD + "Actions left: " + (int) SkyBlockUtils.saves.Enchanting.actionsLeft);
				} else if(selectedSkill == Skill.FARMING) {
					tooltip.add(EnumChatFormatting.BOLD + "Farming " + SkyBlockUtils.saves.Farming.level);
					tooltip.add(EnumChatFormatting.GOLD + "XP: " + SkyBlockUtils.saves.Farming.currentXP + " / " + SkyBlockUtils.saves.Farming.neededXP);
					tooltip.add(EnumChatFormatting.GOLD + "XP/hr: " + + SkyBlockUtils.saves.Farming.xphour);
					tooltip.add(EnumChatFormatting.GOLD + "Actions left: " + (int) SkyBlockUtils.saves.Farming.actionsLeft);
				} else if(selectedSkill == Skill.FISHING) {
					tooltip.add(EnumChatFormatting.BOLD + "Fishing " + SkyBlockUtils.saves.Fishing.level);
					tooltip.add(EnumChatFormatting.GOLD + "XP: " + SkyBlockUtils.saves.Fishing.currentXP + " / " + SkyBlockUtils.saves.Fishing.neededXP);
					tooltip.add(EnumChatFormatting.GOLD + "XP/hr: " + SkyBlockUtils.saves.Fishing.xphour);
					tooltip.add(EnumChatFormatting.GOLD + "Actions left: " + (int) SkyBlockUtils.saves.Fishing.actionsLeft);
				} else if(selectedSkill == Skill.FORAGING) {
					tooltip.add(EnumChatFormatting.BOLD + "Foraging " + SkyBlockUtils.saves.Foraging.level);
					tooltip.add(EnumChatFormatting.GOLD + "XP: " + SkyBlockUtils.saves.Foraging.currentXP + " / " + SkyBlockUtils.saves.Foraging.neededXP);
					tooltip.add(EnumChatFormatting.GOLD + "XP/hr: " + SkyBlockUtils.saves.Foraging.xphour);
					tooltip.add(EnumChatFormatting.GOLD + "Actions left: " + (int) SkyBlockUtils.saves.Foraging.actionsLeft);
				} else if(selectedSkill == Skill.TAMING) {
					tooltip.add(EnumChatFormatting.BOLD + "Taming " + SkyBlockUtils.saves.Taming.level);
					tooltip.add(EnumChatFormatting.GOLD + "XP: " + SkyBlockUtils.saves.Taming.currentXP + " / " + SkyBlockUtils.saves.Taming.neededXP);
					tooltip.add(EnumChatFormatting.GOLD + "XP/hr: " + SkyBlockUtils.saves.Taming.xphour);
					tooltip.add(EnumChatFormatting.GOLD + "Actions left: " + (int) SkyBlockUtils.saves.Taming.actionsLeft);
				} else if(selectedSkill == Skill.MINING) {
					tooltip.add(EnumChatFormatting.BOLD + "Mining " + SkyBlockUtils.saves.Mining.level);
					tooltip.add(EnumChatFormatting.GOLD + "XP: " + SkyBlockUtils.saves.Mining.currentXP + " / " + SkyBlockUtils.saves.Mining.neededXP);
					tooltip.add(EnumChatFormatting.GOLD + "XP/hr: " + SkyBlockUtils.saves.Mining.xphour);
					tooltip.add(EnumChatFormatting.GOLD + "Actions left: " + (int) SkyBlockUtils.saves.Mining.actionsLeft);
				}
				
				if(SkyBlockUtils.saves.skillInfoVisible) {
					drawHoveringText(tooltip, x_skillXPDisplay + 18, y_skillXPDisplay + 80);
				} else {
					drawHoveringText(tooltip, mouseX, mouseY);
				}
			}
			
			if(SkyBlockUtils.saves.skillInfoVisible) {
				if(selectedSkill == Skill.COMBAT) {
					fr.drawString("XP: " + SkyBlockUtils.saves.Combat.currentXP + " XP/hr: " + SkyBlockUtils.saves.Combat.xphour + " Actions Left: " + SkyBlockUtils.saves.Combat.actionsLeft, x_skillXPDisplay - 50, y_skillXPDisplay + 50, 0xAA00AA);
				}
			}
			
			String fullTexture = "EMPTY";
			String emptyTexture = "EMPTY";
			boolean big = false;
			
			if(selectedSkill == Skill.MINING && SkyBlockUtils.saves.Mining.neededXP != 0) {
				fullTexture = "skyblockutils:textures/icons/mining_full.png";
				emptyTexture = "skyblockutils:textures/icons/mining_empty.png";
				width = SkyBlockUtils.saves.Mining.currentXP / SkyBlockUtils.saves.Mining.neededXP * 16;
			} else if(selectedSkill == Skill.COMBAT && SkyBlockUtils.saves.Combat.neededXP != 0) {
				big = true;
				fullTexture = "skyblockutils:textures/icons/combat_full.png";
				emptyTexture = "skyblockutils:textures/icons/combat_empty.png";
				width = SkyBlockUtils.saves.Combat.currentXP / SkyBlockUtils.saves.Combat.neededXP * 18;
			} else if(selectedSkill == Skill.ENCHANTING && SkyBlockUtils.saves.Enchanting.neededXP != 0) {
				big = true;
				fullTexture = "skyblockutils:textures/icons/enchanting_full.png";
				emptyTexture = "skyblockutils:textures/icons/enchanting_empty.png";
				width = SkyBlockUtils.saves.Enchanting.currentXP / SkyBlockUtils.saves.Enchanting.neededXP * 18;
			} else if(selectedSkill == Skill.FARMING && SkyBlockUtils.saves.Farming.neededXP != 0) {
				fullTexture = "skyblockutils:textures/icons/farming_full.png";
				emptyTexture = "skyblockutils:textures/icons/farming_empty.png";
				width = SkyBlockUtils.saves.Farming.currentXP / SkyBlockUtils.saves.Farming.neededXP * 16;
			} else if(selectedSkill == Skill.FISHING && SkyBlockUtils.saves.Fishing.neededXP != 0) {
				fullTexture = "skyblockutils:textures/icons/fishing_full.png";
				emptyTexture = "skyblockutils:textures/icons/fishing_empty.png";
				width = SkyBlockUtils.saves.Fishing.currentXP / SkyBlockUtils.saves.Fishing.neededXP * 16;
			} else if(selectedSkill == Skill.TAMING && SkyBlockUtils.saves.Taming.neededXP != 0) {
				fullTexture = "skyblockutils:textures/icons/taming_full.png";
				emptyTexture = "skyblockutils:textures/icons/taming_empty.png";
				width = SkyBlockUtils.saves.Taming.currentXP / SkyBlockUtils.saves.Taming.neededXP * 16;
			} else if(selectedSkill == Skill.ALCHEMY && SkyBlockUtils.saves.Alchemy.neededXP != 0) {
				fullTexture = "skyblockutils:textures/icons/alchemy_full.png";
				emptyTexture = "skyblockutils:textures/icons/alchemy_empty.png";
				width = SkyBlockUtils.saves.Alchemy.currentXP / SkyBlockUtils.saves.Alchemy.neededXP * 16;
			} else if(selectedSkill == Skill.FORAGING && SkyBlockUtils.saves.Foraging.neededXP != 0) {
				fullTexture = "skyblockutils:textures/icons/foraging_full.png";
				emptyTexture = "skyblockutils:textures/icons/foraging_empty.png";
				width = SkyBlockUtils.saves.Foraging.currentXP / SkyBlockUtils.saves.Foraging.neededXP * 16;
			}
			
			// To avoid recoloring the entire ui
			fr.drawStringWithShadow("", 0, 0, 0xFFFFFF);
			
			if(!fullTexture.equals("EMPTY") && !emptyTexture.equals("EMPTY")) {
				if(big) {
					mc.renderEngine.bindTexture(new ResourceLocation(fullTexture));
					mc.ingameGUI.drawScaledCustomSizeModalRect(x_skillXPDisplay + 1, y_skillXPDisplay + 1, 0, 0, (int)width, 18, (int)width, 18, 18, 18);
					fr.drawStringWithShadow("", 0, 0, 0xFFFFFF);
					mc.renderEngine.bindTexture(new ResourceLocation(emptyTexture));
					mc.ingameGUI.drawScaledCustomSizeModalRect(x_skillXPDisplay + 1, y_skillXPDisplay + 1, 0, 0, 18, 18, 18, 18, 18, 18);
				} else {
					mc.renderEngine.bindTexture(new ResourceLocation(fullTexture));
					mc.ingameGUI.drawScaledCustomSizeModalRect(x_skillXPDisplay, y_skillXPDisplay, 0, 0, (int)width, 16, (int)width, 16, 16, 16);
					fr.drawStringWithShadow("", 0, 0, 0xFFFFFF);
					mc.renderEngine.bindTexture(new ResourceLocation(emptyTexture));
					mc.ingameGUI.drawScaledCustomSizeModalRect(x_skillXPDisplay, y_skillXPDisplay, 0, 0, 16, 16, 16, 16, 16, 16);
				}
			}
			
			int posX = 10;
			int posY = 10;
			
			x_skillXPDisplay = (int)(e.resolution.getScaledWidth() * 0.5f) - 8;
			y_skillXPDisplay = 20;
			
			// To avoid recoloring the entire ui
			fr.drawStringWithShadow("", 0, 0, 0xFFFFFF);
			mc.renderEngine.bindTexture(new ResourceLocation(entityTexture));
			mc.ingameGUI.drawScaledCustomSizeModalRect(posX, posY, 0, 0, 16, 16, 16, 16, 16, 16);
			
			FontRenderer fontRenderer = Minecraft.getMinecraft().fontRendererObj;
			
			ScaledResolution resolution = new ScaledResolution(Minecraft.getMinecraft());
			mouseX = Mouse.getEventX() * resolution.getScaledWidth() / (Minecraft.getMinecraft()).displayWidth;
			mouseY = resolution.getScaledHeight() - Mouse.getEventY() * resolution.getScaledHeight() / (Minecraft.getMinecraft()).displayHeight - 1;
			
			for(KillStat ks: SkyBlockUtils.saves.KillStats) {
				if(ks.entity.equals(SkyBlockUtils.saves.selectedEntity)) {
					fontRenderer.drawStringWithShadow(ks.value + "", posX + 18, posY + 4, 0xAA00AA);
				}
			}
		}
	}
	
	private void drawHoveringText(ArrayList<String> textLines, int x, int y) {
		FontRenderer fontRendererObj = (Minecraft.getMinecraft()).fontRendererObj;
		ScaledResolution resolution = new ScaledResolution(Minecraft.getMinecraft());
		if (!textLines.isEmpty()) {
			GlStateManager.disableRescaleNormal();
			RenderHelper.disableStandardItemLighting();
			GlStateManager.disableLighting();
			GlStateManager.disableDepth();
			int i = 0;
			for (String s : textLines) {
				int j = fontRendererObj.getStringWidth(s);
				if (j > i)
					i = j;
			}
			int l1 = x + 12;
			int i2 = y - 12;
			int k = 8;
			if (textLines.size() > 1)
				k += 2 + (textLines.size() - 1) * 10;
			if (l1 + i > resolution.getScaledWidth())
				l1 -= 28 + i;
			if (i2 + k + 6 > resolution.getScaledHeight())
				i2 = resolution.getScaledHeight() - k - 6;
			zLevel = 300.0F;
			int l = -267386864;
			drawGradientRect(l1 - 3, i2 - 4, l1 + i + 3, i2 - 3, l, l);
			drawGradientRect(l1 - 3, i2 + k + 3, l1 + i + 3, i2 + k + 4, l, l);
			drawGradientRect(l1 - 3, i2 - 3, l1 + i + 3, i2 + k + 3, l, l);
			drawGradientRect(l1 - 4, i2 - 3, l1 - 3, i2 + k + 3, l, l);
			drawGradientRect(l1 + i + 3, i2 - 3, l1 + i + 4, i2 + k + 3, l, l);
			int i1 = 1347420415;
			int j1 = (i1 & 0xFEFEFE) >> 1 | i1 & 0xFF000000;
			drawGradientRect(l1 - 3, i2 - 3 + 1, l1 - 3 + 1, i2 + k + 3 - 1, i1, j1);
			drawGradientRect(l1 + i + 2, i2 - 3 + 1, l1 + i + 3, i2 + k + 3 - 1, i1, j1);
			drawGradientRect(l1 - 3, i2 - 3, l1 + i + 3, i2 - 3 + 1, i1, i1);
			drawGradientRect(l1 - 3, i2 + k + 2, l1 + i + 3, i2 + k + 3, j1, j1);
			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
			for (int k1 = 0; k1 < textLines.size(); k1++) {
				String s1 = textLines.get(k1);
				fontRendererObj.drawStringWithShadow(s1, l1, i2, -1);
				if (k1 == 0)
					i2 += 2;
				i2 += 10;
			}
			zLevel = 0.0F;
			GlStateManager.enableRescaleNormal();
			GlStateManager.enableLighting();
			RenderHelper.enableStandardItemLighting();
			GlStateManager.enableDepth();
		}
	}
	
	protected void drawGradientRect(int left, int top, int right, int bottom, int startColor, int endColor) {
		float f = (startColor >> 24 & 0xFF) / 255.0F;
		float f1 = (startColor >> 16 & 0xFF) / 255.0F;
		float f2 = (startColor >> 8 & 0xFF) / 255.0F;
		float f3 = (startColor & 0xFF) / 255.0F;
		float f4 = (endColor >> 24 & 0xFF) / 255.0F;
		float f5 = (endColor >> 16 & 0xFF) / 255.0F;
		float f6 = (endColor >> 8 & 0xFF) / 255.0F;
		float f7 = (endColor & 0xFF) / 255.0F;
		GlStateManager.disableTexture2D();
		GlStateManager.enableBlend();
		GlStateManager.disableAlpha();
		GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
		GlStateManager.shadeModel(7425);
		Tessellator tessellator = Tessellator.getInstance();
		WorldRenderer worldrenderer = tessellator.getWorldRenderer();
		worldrenderer.begin(7, DefaultVertexFormats.POSITION_COLOR);
		worldrenderer.pos(right, top, zLevel).color(f1, f2, f3, f).endVertex();
		worldrenderer.pos(left, top, zLevel).color(f1, f2, f3, f).endVertex();
		worldrenderer.pos(left, bottom, zLevel).color(f5, f6, f7, f4).endVertex();
		worldrenderer.pos(right, bottom, zLevel).color(f5, f6, f7, f4).endVertex();
		tessellator.draw();
		GlStateManager.shadeModel(7424);
		GlStateManager.disableBlend();
		GlStateManager.enableAlpha();
		GlStateManager.enableTexture2D();
	}
	
	@SubscribeEvent
	public void onChatReceived(ClientChatReceivedEvent e) {
		if(e.type == 2) {
			String s = EnumChatFormatting.getTextWithoutFormattingCodes(e.message.getUnformattedText());
			//System.out.println("Type: " + e.type + " Msg: " + s);
			String[] hotbarInfo = s.split("     ");
			String actualMessage = (hotbarInfo.length == 3) ? hotbarInfo[1] : hotbarInfo[0];
			if(actualMessage.startsWith("+") && actualMessage.endsWith(")")) {
				String[] expMessageSplit = actualMessage.replace(",", "").split(" ");
				String skillName = expMessageSplit[1];
				
				if(lastXP != Double.parseDouble(expMessageSplit[2].replace("(", "").replace(")", "").split("/")[0])) {
					if(skillName.equals("Combat")) {
						reset(Skill.COMBAT);
						
						combat_currentTime = System.currentTimeMillis();
						
						selectedSkill = Skill.COMBAT;
						SkyBlockUtils.saves.Combat.xpGained = Double.parseDouble(expMessageSplit[0].replace("+", ""));
						SkyBlockUtils.saves.Combat.currentXP = Double.parseDouble(expMessageSplit[2].replace("(", "").replace(")", "").split("/")[0]);
						SkyBlockUtils.saves.Combat.neededXP = Double.parseDouble(expMessageSplit[2].replace("(", "").replace(")", "").split("/")[1]);
						SkyBlockUtils.saves.Combat.xpLeft = SkyBlockUtils.saves.Combat.neededXP - SkyBlockUtils.saves.Combat.currentXP;
						SkyBlockUtils.saves.Combat.actionsLeft = SkyBlockUtils.saves.Combat.xpLeft / SkyBlockUtils.saves.Combat.xpGained;
						
						combat_currentXP = SkyBlockUtils.saves.Combat.currentXP;
						
						SkyBlockUtils.saves.Combat.xphour = (float) (((combat_currentXP - combat_lastXP) / (combat_currentTime - combat_lastTime)) * 3600000);
						
						System.out.println("LastTime: " + System.currentTimeMillis());
						
						combat_lastXP = combat_currentXP;
						combat_lastTime = combat_currentTime;
					}
					if(skillName.equals("Enchanting")) {
						reset(Skill.ENCHANTING);
						
						enchanting_currentTime = System.currentTimeMillis();
						
						selectedSkill = Skill.ENCHANTING;
						SkyBlockUtils.saves.Enchanting.xpGained = Double.parseDouble(expMessageSplit[0].replace("+", ""));
						SkyBlockUtils.saves.Enchanting.currentXP = Double.parseDouble(expMessageSplit[2].replace("(", "").replace(")", "").split("/")[0]);
						SkyBlockUtils.saves.Enchanting.neededXP = Double.parseDouble(expMessageSplit[2].replace("(", "").replace(")", "").split("/")[1]);
						SkyBlockUtils.saves.Enchanting.xpLeft = SkyBlockUtils.saves.Enchanting.neededXP - SkyBlockUtils.saves.Enchanting.currentXP;
						SkyBlockUtils.saves.Enchanting.actionsLeft = SkyBlockUtils.saves.Enchanting.xpLeft / SkyBlockUtils.saves.Enchanting.xpGained;
						SkyBlockUtils.saves.Enchanting.lastXP = System.currentTimeMillis();
						
						enchanting_currentXP = SkyBlockUtils.saves.Enchanting.currentXP;
						
						SkyBlockUtils.saves.Enchanting.xphour = (float) (((enchanting_currentXP - enchanting_lastXP) / (enchanting_currentTime - enchanting_lastTime)) * 3600000);
						
						System.out.println("LastTime: " + System.currentTimeMillis());
						
						enchanting_lastXP = enchanting_currentXP;
						enchanting_lastTime = enchanting_currentTime;
					}
					if(skillName.equals("Mining")) {
						reset(Skill.MINING);
						
						mining_currentTime = System.currentTimeMillis();
						
						selectedSkill = Skill.MINING;
						SkyBlockUtils.saves.Mining.xpGained = Double.parseDouble(expMessageSplit[0].replace("+", ""));
						SkyBlockUtils.saves.Mining.currentXP = Double.parseDouble(expMessageSplit[2].replace("(", "").replace(")", "").split("/")[0]);
						SkyBlockUtils.saves.Mining.neededXP = Double.parseDouble(expMessageSplit[2].replace("(", "").replace(")", "").split("/")[1]);
						SkyBlockUtils.saves.Mining.xpLeft = SkyBlockUtils.saves.Mining.neededXP - SkyBlockUtils.saves.Mining.currentXP;
						SkyBlockUtils.saves.Mining.actionsLeft = SkyBlockUtils.saves.Mining.xpLeft / SkyBlockUtils.saves.Mining.xpGained;
						
						mining_currentXP = SkyBlockUtils.saves.Mining.currentXP;
						
						SkyBlockUtils.saves.Mining.xphour = (float) (((mining_currentXP - mining_lastXP) / (mining_currentTime - mining_lastTime)) * 3600000);
						
						System.out.println("LastTime: " + System.currentTimeMillis());
						
						mining_lastXP = mining_currentXP;
						mining_lastTime = mining_currentTime;
					}
					if(skillName.equals("Foraging")) {
						reset(Skill.FORAGING);
						
						foraging_currentTime = System.currentTimeMillis();
						
						selectedSkill = Skill.FORAGING;
						SkyBlockUtils.saves.Foraging.xpGained = Double.parseDouble(expMessageSplit[0].replace("+", ""));
						SkyBlockUtils.saves.Foraging.currentXP = Double.parseDouble(expMessageSplit[2].replace("(", "").replace(")", "").split("/")[0]);
						SkyBlockUtils.saves.Foraging.neededXP = Double.parseDouble(expMessageSplit[2].replace("(", "").replace(")", "").split("/")[1]);
						SkyBlockUtils.saves.Foraging.xpLeft = SkyBlockUtils.saves.Foraging.neededXP - SkyBlockUtils.saves.Foraging.currentXP;
						SkyBlockUtils.saves.Foraging.actionsLeft = SkyBlockUtils.saves.Foraging.xpLeft / SkyBlockUtils.saves.Foraging.xpGained;
						SkyBlockUtils.saves.Foraging.lastXP = System.currentTimeMillis();
						
						foraging_currentXP = SkyBlockUtils.saves.Foraging.currentXP;
						
						SkyBlockUtils.saves.Foraging.xphour = (float) (((foraging_currentXP - foraging_lastXP) / (foraging_currentTime - foraging_lastTime)) * 3600000);
						
						System.out.println("LastTime: " + System.currentTimeMillis());
						
						foraging_lastXP = foraging_currentXP;
						foraging_lastTime = foraging_currentTime;
					}
					if(skillName.equals("Taming")) {
						reset(Skill.TAMING);
						
						taming_currentTime = System.currentTimeMillis();
						
						selectedSkill = Skill.TAMING;
						SkyBlockUtils.saves.Taming.xpGained = Double.parseDouble(expMessageSplit[0].replace("+", ""));
						SkyBlockUtils.saves.Taming.currentXP = Double.parseDouble(expMessageSplit[2].replace("(", "").replace(")", "").split("/")[0]);
						SkyBlockUtils.saves.Taming.neededXP = Double.parseDouble(expMessageSplit[2].replace("(", "").replace(")", "").split("/")[1]);
						SkyBlockUtils.saves.Taming.xpLeft = SkyBlockUtils.saves.Taming.neededXP - SkyBlockUtils.saves.Taming.currentXP;
						SkyBlockUtils.saves.Taming.actionsLeft = SkyBlockUtils.saves.Taming.xpLeft / SkyBlockUtils.saves.Taming.xpGained;
						SkyBlockUtils.saves.Taming.lastXP = System.currentTimeMillis();
						
						taming_currentXP = SkyBlockUtils.saves.Taming.currentXP;
						
						SkyBlockUtils.saves.Taming.xphour = (float) (((taming_currentXP - taming_lastXP) / (taming_currentTime - taming_lastTime)) * 3600000);
						
						System.out.println("LastTime: " + System.currentTimeMillis());
						
						taming_lastXP = taming_currentXP;
						taming_lastTime = taming_currentTime;
					}
					if(skillName.equals("Alchemy")) {
						reset(Skill.ALCHEMY);
						
						alchemy_currentTime = System.currentTimeMillis();
						
						selectedSkill = Skill.ALCHEMY;
						SkyBlockUtils.saves.Alchemy.xpGained = Double.parseDouble(expMessageSplit[0].replace("+", ""));
						SkyBlockUtils.saves.Alchemy.currentXP = Double.parseDouble(expMessageSplit[2].replace("(", "").replace(")", "").split("/")[0]);
						SkyBlockUtils.saves.Alchemy.neededXP = Double.parseDouble(expMessageSplit[2].replace("(", "").replace(")", "").split("/")[1]);
						SkyBlockUtils.saves.Alchemy.xpLeft = SkyBlockUtils.saves.Alchemy.neededXP - SkyBlockUtils.saves.Alchemy.currentXP;
						SkyBlockUtils.saves.Alchemy.actionsLeft = SkyBlockUtils.saves.Alchemy.xpLeft / SkyBlockUtils.saves.Alchemy.xpGained;
						SkyBlockUtils.saves.Alchemy.lastXP = System.currentTimeMillis();
						
						alchemy_currentXP = SkyBlockUtils.saves.Alchemy.currentXP;
						
						SkyBlockUtils.saves.Alchemy.xphour = (float) (((alchemy_currentXP - alchemy_lastXP) / (alchemy_currentTime - alchemy_lastTime)) * 3600000);
						
						System.out.println("LastTime: " + System.currentTimeMillis());
						
						alchemy_lastXP = alchemy_currentXP;
						alchemy_lastTime = alchemy_currentTime;
					}
					if(skillName.equals("Fishing")) {
						reset(Skill.FISHING);
						
						fishing_currentTime = System.currentTimeMillis();
						
						selectedSkill = Skill.FISHING;
						SkyBlockUtils.saves.Fishing.xpGained = Double.parseDouble(expMessageSplit[0].replace("+", ""));
						SkyBlockUtils.saves.Fishing.currentXP = Double.parseDouble(expMessageSplit[2].replace("(", "").replace(")", "").split("/")[0]);
						SkyBlockUtils.saves.Fishing.neededXP = Double.parseDouble(expMessageSplit[2].replace("(", "").replace(")", "").split("/")[1]);
						SkyBlockUtils.saves.Fishing.xpLeft = SkyBlockUtils.saves.Fishing.neededXP - SkyBlockUtils.saves.Fishing.currentXP;
						SkyBlockUtils.saves.Fishing.actionsLeft = SkyBlockUtils.saves.Fishing.xpLeft / SkyBlockUtils.saves.Fishing.xpGained;
						SkyBlockUtils.saves.Fishing.lastXP = System.currentTimeMillis();
						
						fishing_currentXP = SkyBlockUtils.saves.Fishing.currentXP;
						
						SkyBlockUtils.saves.Fishing.xphour = (float) (((fishing_currentXP - fishing_lastXP) / (fishing_currentTime - fishing_lastTime)) * 3600000);

						System.out.println("LastTime: " + System.currentTimeMillis());
						
						fishing_lastXP = fishing_currentXP;
						fishing_lastTime = fishing_currentTime;
					}
					if(skillName.equals("Farming")) {
						reset(Skill.FARMING);
						
						farming_currentTime = System.currentTimeMillis();
						
						selectedSkill = Skill.FARMING;
						SkyBlockUtils.saves.Farming.xpGained = Double.parseDouble(expMessageSplit[0].replace("+", ""));
						SkyBlockUtils.saves.Farming.currentXP = Double.parseDouble(expMessageSplit[2].replace("(", "").replace(")", "").split("/")[0]);
						SkyBlockUtils.saves.Farming.neededXP = Double.parseDouble(expMessageSplit[2].replace("(", "").replace(")", "").split("/")[1]);
						SkyBlockUtils.saves.Farming.xpLeft = SkyBlockUtils.saves.Farming.neededXP - SkyBlockUtils.saves.Farming.currentXP;
						SkyBlockUtils.saves.Farming.actionsLeft = SkyBlockUtils.saves.Farming.xpLeft / SkyBlockUtils.saves.Farming.xpGained;
						SkyBlockUtils.saves.Farming.lastXP = System.currentTimeMillis();
						
						farming_currentXP = SkyBlockUtils.saves.Farming.currentXP;
						
						SkyBlockUtils.saves.Farming.xphour = (float) (((farming_currentXP - farming_lastXP) / (farming_currentTime - farming_lastTime)) * 3600000);

						System.out.println("LastTime: " + System.currentTimeMillis());
						
						farming_lastXP = farming_currentXP;
						farming_lastTime = farming_currentTime;
					}
				}
				lastXP = Double.parseDouble(expMessageSplit[2].replace("(", "").replace(")", "").split("/")[0]);
			}
		}
	}
	
	public void reset(Skill skill) {
		if(skill == Skill.COMBAT) {
			alchemy_currentTime = 1;
			alchemy_currentXP = 1;
			alchemy_lastTime = 1;
			alchemy_lastXP = 1;
			
			enchanting_currentTime = 1;
			enchanting_currentXP = 1;
			enchanting_lastTime = 1;
			enchanting_lastXP = 1;
			
			farming_currentTime = 1;
			farming_currentXP = 1;
			farming_lastTime = 1;
			farming_lastXP = 1;
			
			fishing_currentTime = 1;
			fishing_currentXP = 1;
			fishing_lastTime = 1;
			fishing_lastXP = 1;
			
			foraging_currentTime = 1;
			foraging_currentXP = 1;
			foraging_lastTime = 1;
			foraging_lastXP = 1;
			
			mining_currentTime = 1;
			mining_currentXP = 1;
			mining_lastTime = 1;
			mining_lastXP = 1;
			
			taming_currentTime = 1;
			taming_currentXP = 1;
			taming_lastTime = 1;
		} else if(skill == Skill.ALCHEMY) {
			combat_currentTime = 1;
			combat_currentXP = 1;
			combat_lastTime = 1;
			combat_lastXP = 1;
			
			enchanting_currentTime = 1;
			enchanting_currentXP = 1;
			enchanting_lastTime = 1;
			enchanting_lastXP = 1;
			
			farming_currentTime = 1;
			farming_currentXP = 1;
			farming_lastTime = 1;
			farming_lastXP = 1;
			
			fishing_currentTime = 1;
			fishing_currentXP = 1;
			fishing_lastTime = 1;
			fishing_lastXP = 1;
			
			foraging_currentTime = 1;
			foraging_currentXP = 1;
			foraging_lastTime = 1;
			foraging_lastXP = 1;
			
			mining_currentTime = 1;
			mining_currentXP = 1;
			mining_lastTime = 1;
			mining_lastXP = 1;
			
			taming_currentTime = 1;
			taming_currentXP = 1;
			taming_lastTime = 1;
		} else if(skill == Skill.ENCHANTING)  {
			alchemy_currentTime = 1;
			alchemy_currentXP = 1;
			alchemy_lastTime = 1;
			alchemy_lastXP = 1;
			
			combat_currentTime = 1;
			combat_currentXP = 1;
			combat_lastTime = 1;
			combat_lastXP = 1;
			
			farming_currentTime = 1;
			farming_currentXP = 1;
			farming_lastTime = 1;
			farming_lastXP = 1;
			
			fishing_currentTime = 1;
			fishing_currentXP = 1;
			fishing_lastTime = 1;
			fishing_lastXP = 1;
			
			foraging_currentTime = 1;
			foraging_currentXP = 1;
			foraging_lastTime = 1;
			foraging_lastXP = 1;
			
			mining_currentTime = 1;
			mining_currentXP = 1;
			mining_lastTime = 1;
			mining_lastXP = 1;
			
			taming_currentTime = 1;
			taming_currentXP = 1;
			taming_lastTime = 1;
		} else if(skill == Skill.FARMING) {
			alchemy_currentTime = 1;
			alchemy_currentXP = 1;
			alchemy_lastTime = 1;
			alchemy_lastXP = 1;
			
			combat_currentTime = 1;
			combat_currentXP = 1;
			combat_lastTime = 1;
			combat_lastXP = 1;
			
			enchanting_currentTime = 1;
			enchanting_currentXP = 1;
			enchanting_lastTime = 1;
			enchanting_lastXP = 1;
			
			fishing_currentTime = 1;
			fishing_currentXP = 1;
			fishing_lastTime = 1;
			fishing_lastXP = 1;
			
			foraging_currentTime = 1;
			foraging_currentXP = 1;
			foraging_lastTime = 1;
			foraging_lastXP = 1;
			
			mining_currentTime = 1;
			mining_currentXP = 1;
			mining_lastTime = 1;
			mining_lastXP = 1;
			
			taming_currentTime = 1;
			taming_currentXP = 1;
			taming_lastTime = 1;
		} else if(skill == Skill.FISHING) {
			alchemy_currentTime = 1;
			alchemy_currentXP = 1;
			alchemy_lastTime = 1;
			alchemy_lastXP = 1;
			
			combat_currentTime = 1;
			combat_currentXP = 1;
			combat_lastTime = 1;
			combat_lastXP = 1;
			
			enchanting_currentTime = 1;
			enchanting_currentXP = 1;
			enchanting_lastTime = 1;
			enchanting_lastXP = 1;
			
			farming_currentTime = 1;
			farming_currentXP = 1;
			farming_lastTime = 1;
			farming_lastXP = 1;
			
			foraging_currentTime = 1;
			foraging_currentXP = 1;
			foraging_lastTime = 1;
			foraging_lastXP = 1;
			
			mining_currentTime = 1;
			mining_currentXP = 1;
			mining_lastTime = 1;
			mining_lastXP = 1;
			
			taming_currentTime = 1;
			taming_currentXP = 1;
			taming_lastTime = 1;
		} else if(skill == Skill.FORAGING) {
			alchemy_currentTime = 1;
			alchemy_currentXP = 1;
			alchemy_lastTime = 1;
			alchemy_lastXP = 1;
			
			combat_currentTime = 1;
			combat_currentXP = 1;
			combat_lastTime = 1;
			combat_lastXP = 1;
			
			enchanting_currentTime = 1;
			enchanting_currentXP = 1;
			enchanting_lastTime = 1;
			enchanting_lastXP = 1;
			
			farming_currentTime = 1;
			farming_currentXP = 1;
			farming_lastTime = 1;
			farming_lastXP = 1;
			
			fishing_currentTime = 1;
			fishing_currentXP = 1;
			fishing_lastTime = 1;
			fishing_lastXP = 1;
			
			mining_currentTime = 1;
			mining_currentXP = 1;
			mining_lastTime = 1;
			mining_lastXP = 1;
			
			taming_currentTime = 1;
			taming_currentXP = 1;
			taming_lastTime = 1;
		} else if(skill == Skill.MINING) {
			alchemy_currentTime = 1;
			alchemy_currentXP = 1;
			alchemy_lastTime = 1;
			alchemy_lastXP = 1;
			
			combat_currentTime = 1;
			combat_currentXP = 1;
			combat_lastTime = 1;
			combat_lastXP = 1;
			
			enchanting_currentTime = 1;
			enchanting_currentXP = 1;
			enchanting_lastTime = 1;
			enchanting_lastXP = 1;
			
			farming_currentTime = 1;
			farming_currentXP = 1;
			farming_lastTime = 1;
			farming_lastXP = 1;
			
			fishing_currentTime = 1;
			fishing_currentXP = 1;
			fishing_lastTime = 1;
			fishing_lastXP = 1;
			
			foraging_currentTime = 1;
			foraging_currentXP = 1;
			foraging_lastTime = 1;
			foraging_lastXP = 1;
			
			taming_currentTime = 1;
			taming_currentXP = 1;
			taming_lastTime = 1;
			taming_lastXP = 1;
		} else if(skill == Skill.TAMING) {
			alchemy_currentTime = 1;
			alchemy_currentXP = 1;
			alchemy_lastTime = 1;
			alchemy_lastXP = 1;
			
			combat_currentTime = 1;
			combat_currentXP = 1;
			combat_lastTime = 1;
			combat_lastXP = 1;
			
			enchanting_currentTime = 1;
			enchanting_currentXP = 1;
			enchanting_lastTime = 1;
			enchanting_lastXP = 1;
			
			farming_currentTime = 1;
			farming_currentXP = 1;
			farming_lastTime = 1;
			farming_lastXP = 1;
			
			fishing_currentTime = 1;
			fishing_currentXP = 1;
			fishing_lastTime = 1;
			fishing_lastXP = 1;
			
			foraging_currentTime = 1;
			foraging_currentXP = 1;
			foraging_lastTime = 1;
			foraging_lastXP = 1;
			
			mining_currentTime = 1;
			mining_currentXP = 1;
			mining_lastTime = 1;
			mining_lastXP = 1;
		}
	}
}
