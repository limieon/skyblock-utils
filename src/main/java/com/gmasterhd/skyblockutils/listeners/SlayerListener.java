package com.gmasterhd.skyblockutils.listeners;

import com.gmasterhd.skyblockutils.SkyBlockUtils;
import com.gmasterhd.skyblockutils.jsons.PersistentSkill;
import com.gmasterhd.skyblockutils.jsons.PersistentValues;
import com.gmasterhd.skyblockutils.jsons.SlayerItem;
import com.gmasterhd.skyblockutils.jsons.config.Config;
import com.gmasterhd.skyblockutils.jsons.config.Slayer;
import com.gmasterhd.skyblockutils.utils.Toolbox;

import net.minecraft.client.Minecraft;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.client.event.ClientChatReceivedEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.List;

public class SlayerListener {
	public Slayer lastSlayer;
	public int lastSlayerIndex = -1;
	
	@SubscribeEvent
	public void onChatReceived(ClientChatReceivedEvent e) {
		if(e.type == 0 ||  e.type == 1) {
			String msg = EnumChatFormatting.getTextWithoutFormattingCodes(e.message.getUnformattedText());
			
			System.out.println(msg);
			
			List<Slayer> slayers = SkyBlockUtils.config.Slayers;
			
			for(int x = 0; x < slayers.size(); ++x) {
				if(msg.contains(slayers.get(x).chatTrigger) && Toolbox.scoreboardContains(Minecraft.getMinecraft().theWorld.getScoreboard(), slayers.get(x).scoreTrigger)) {
					lastSlayer = slayers.get(x);
					lastSlayerIndex = x;
					
					int index = -1;
					
					boolean found = false;
					for(int y = 0; y < SkyBlockUtils.saves.Slayers.size(); ++y) {
						if(SkyBlockUtils.saves.Slayers.get(y).slayerName.equals(lastSlayer.name)) {
							found = true;
							index = y;
						}
					}
					
					if(found) {
						SkyBlockUtils.saves.Slayers.set(lastSlayerIndex, new com.gmasterhd.skyblockutils.jsons.Slayer(SkyBlockUtils.saves.Slayers.get(index).slayerName, ++SkyBlockUtils.saves.Slayers.get(index).kills));
						SkyBlockUtils.file.save(SkyBlockUtils.saves);
					} else {
						System.out.println("No Slayer could be found! Add " + lastSlayer.name + " to your saves file!");
					}
					
					x = 999;
					break;
				}
			}
			
			if(msg.contains("RARE DROP") || msg.contains("VERY RARE DROP") || msg.contains("CRAZY RARE DROP")) {
				int i_configDropIndex = -1;
				int i_dropIndex = -1;
				int i_slayerIndex = -1;
				
				for(int x = 0; x < lastSlayer.Drops.size(); ++x) {
					if(msg.contains(lastSlayer.Drops.get(x).chatTrigger)) {
						i_configDropIndex = x;
					}
				}
				
				boolean found = false;
				
				for(int y = 0; y < SkyBlockUtils.saves.Slayers.size(); ++y) {
					if(SkyBlockUtils.saves.Slayers.get(y).slayerName.equals(SkyBlockUtils.config.Slayers.get(lastSlayerIndex).name)) {
						i_slayerIndex = y;
					}
				}
				
				for(int z = 0; z < SkyBlockUtils.saves.Slayers.get(lastSlayerIndex).items.size(); ++z) {
					SlayerItem i = SkyBlockUtils.saves.Slayers.get(lastSlayerIndex).items.get(z);
					
					if(msg.contains(i.name)) {
						i_dropIndex = z;
						found = true;
						break;
					}
				}
				
				if(found) {
					com.gmasterhd.skyblockutils.jsons.Slayer s = SkyBlockUtils.saves.Slayers.get(i_slayerIndex);
					s.items.set(i_dropIndex, new SlayerItem(SkyBlockUtils.saves.Slayers.get(i_slayerIndex).items.get(i_dropIndex).name, ++SkyBlockUtils.saves.Slayers.get(i_slayerIndex).items.get(i_dropIndex).count));
					SkyBlockUtils.saves.Slayers.set(i_slayerIndex, s);
					SkyBlockUtils.file.save(SkyBlockUtils.saves);
				} else {
					com.gmasterhd.skyblockutils.jsons.Slayer s = SkyBlockUtils.saves.Slayers.get(i_slayerIndex);
					s.items.add(new SlayerItem(lastSlayer.Drops.get(i_configDropIndex).name, 1));
					SkyBlockUtils.saves.Slayers.set(i_slayerIndex, s);
					SkyBlockUtils.file.save(SkyBlockUtils.saves);
				}
			}
		}
	}
}
