package com.gmasterhd.skyblockutils.utils;

import com.gmasterhd.skyblockutils.SkyBlockUtils;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.minecraft.client.Minecraft;
import net.minecraft.scoreboard.*;
import net.minecraftforge.common.MinecraftForge;

import java.util.*;
import java.util.stream.Collectors;

public class Toolbox {
	public static String getUUIDByUsername(String username) {
		return APIRequests.request("https://api.mojang.com/users/profiles/minecraft/" + username).get("id").getAsString();
	}
	
	public static String getSkyBlockProfileID(String username, String name) {
		JsonObject playerStats = APIRequests.request("https://api.hypixel.net/player?key=" + SkyBlockUtils.saves.apiKey + "&uuid=" + getUUIDByUsername(username));
		List<String> keys = new ArrayList();
		JsonObject profiles = playerStats.getAsJsonObject("player").getAsJsonObject("stats").getAsJsonObject("SkyBlock").getAsJsonObject("profiles");
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(profiles.getAsString());
		JsonObject obj = element.getAsJsonObject(); //since you know it's a JsonObject
		Set<Map.Entry<String, JsonElement>> entries = obj.entrySet();//will return members of your object
		for(Map.Entry<String, JsonElement> entry : entries) {
			keys.add(entry.getKey());
		}
		for(String k : keys) {
			String cuteName = profiles.getAsJsonObject(k).get("cute_name").getAsString();
			System.out.println(k + " ProfileName: " + cuteName);
			if(cuteName.equals(name)) {
				return k;
			}
		}
		
		return "";
	}
	public static String getSkyBlockProfileID(String username) {
		JsonObject playerStats = APIRequests.request("https://api.hypixel.net/player?key=" + SkyBlockUtils.saves.apiKey + "&uuid=" + getUUIDByUsername(username));
		List<String> keys = new ArrayList();
		JsonObject player = playerStats.getAsJsonObject("player");
		JsonObject stats = player.getAsJsonObject("stats");
		JsonObject SkyBlock = stats.getAsJsonObject("SkyBlock");
		JsonObject profiles = SkyBlock.getAsJsonObject("profiles");
		Set<Map.Entry<String, JsonElement>> entries = profiles.entrySet();//will return members of your object
		for(Map.Entry<String, JsonElement> entry : entries) {
			System.out.println("Key: " + entry.getKey());
			keys.add(entry.getKey());
		}
		
		System.out.println(profiles.getAsJsonObject(keys.get(0)).get("cute_name").getAsString());
		return profiles.getAsJsonObject(keys.get(0)).get("profile_id").getAsString();
	}
	
	public static JsonObject getSkyBlockStats(String username) {
		System.out.println("Username: " + username + ", Key: " + SkyBlockUtils.saves.apiKey + ", Profile: " + getSkyBlockProfileID(username));
		
		JsonObject skyblockstats = APIRequests.request("https://api.hypixel.net/skyblock/profile?key=" + SkyBlockUtils.saves.apiKey + "&profile=" + getSkyBlockProfileID(username));
		JsonObject profile = skyblockstats.getAsJsonObject("profile");
		JsonObject members = profile.getAsJsonObject("members");
		JsonObject member = members.getAsJsonObject(getUUIDByUsername(username).replace("-", ""));
		
		System.out.println("Member: " + member.toString());
		return member;
	}
	public static JsonObject getSkyBlockStats(String username, String profileName) {
		JsonObject skyblockstats = APIRequests.request("https://api.hypixel.net/skyblock/profile?key=" + SkyBlockUtils.saves.apiKey + "&profile=" + getSkyBlockProfileID(username, profileName));
		JsonObject profile = skyblockstats.getAsJsonObject("profile");
		JsonObject members = profile.getAsJsonObject("members");
		JsonObject member = members.getAsJsonObject(getUUIDByUsername(username).replace("-", ""));
		
		System.out.println("Member: " + member.toString());
		return member;
	}
	
	public static List<String> getSidebarLines(Scoreboard board) {
		List<String> lines = new ArrayList();
		// Grab the main objective of the scoreboard.
		ScoreObjective objective = board.getObjectiveInDisplaySlot(1);
		
		List<Score> scores = (List<Score>)board.getSortedScores(objective);
		
		scores = (List<Score>)scores.stream().filter(input -> (input.getPlayerName() != null && !input.getPlayerName().startsWith("#"))).skip(Math.max(scores.size() - 15, 0)).collect(Collectors.toList());
		
		Formatter formatter = new Formatter();
		
		Collections.reverse(scores);
		for (Score score : scores) {
			int width = 30;
			ScorePlayerTeam scoreplayerteam = board.getPlayersTeam(score.getPlayerName());
			String playerName = ScorePlayerTeam.formatPlayerName((Team)scoreplayerteam, score.getPlayerName());
			playerName = RegexUtils.strip(playerName, RegexUtils.SIDEBAR_PLAYER_NAME_PATTERN);
			//if (stripControlCodes)
			//	playerName = StringUtils.func_76338_a(playerName);
			int points = score.getScorePoints();
			width = Math.max(width, (playerName + " " + points).length());
			formatter.format("%-" + width + "." + width + "s %d%n", new Object[] { playerName, Integer.valueOf(points) });
			
			lines.add(playerName);
			
			System.out.println("Width: " + width + " PlayerName: " + playerName);
		}
		
		return lines;
	}
	
	public static boolean scoreboardContains(Scoreboard board, String contains) {
		List<String> boardLines = getSidebarLines(board);
		
		boolean found = false;
		for(String s: boardLines) {
			if(s.contains(contains)) {
				found = true;
			}
		}
		
		return found;
	}
}
