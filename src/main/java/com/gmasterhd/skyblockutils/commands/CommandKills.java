package com.gmasterhd.skyblockutils.commands;

import com.gmasterhd.skyblockutils.SkyBlockUtils;

import com.gmasterhd.skyblockutils.jsons.KillStat;
import com.gmasterhd.skyblockutils.listeners.RenderListener;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;

public class CommandKills extends CommandBase {
	@Override
	public String getCommandName() {
		return "sbukills";
	}
	
	@Override
	public String getCommandUsage(ICommandSender sender) {
		return "/sbukills [entity]";
	}
	
	@Override
	public void processCommand(ICommandSender sender, String[] args) throws CommandException {
		if(args.length > 0) {
			String kill = "";
			boolean first = true;
			for(String s: args) {
				if(first) {
					kill += s;
					first = false;
				} else {
					kill += " ";
					kill += s;
				}
			}
			
			SkyBlockUtils.saves.selectedEntity = kill;
			RenderListener.changeEntity(SkyBlockUtils.saves.selectedEntity);
			
			sender.addChatMessage(new ChatComponentText("Set kill listener to " + kill));
			
			boolean found = false;
			for(KillStat ks: SkyBlockUtils.saves.KillStats) {
				if(ks.entity.equals(kill)) {
					found = true;
				}
			}
			
			if(!found) {
				SkyBlockUtils.saves.KillStats.add(new KillStat(kill, 0));
			}
		} else {
			sender.addChatMessage(new ChatComponentText("Current kill listener: " + SkyBlockUtils.saves.selectedEntity));
		}
	}
	
	@Override
	public boolean canCommandSenderUseCommand(ICommandSender sender) {
		return true;
	}
}
