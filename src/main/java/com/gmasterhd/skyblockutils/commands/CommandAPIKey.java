package com.gmasterhd.skyblockutils.commands;

import com.gmasterhd.skyblockutils.SkyBlockUtils;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;

public class CommandAPIKey extends CommandBase {
	@Override
	public String getCommandName() {
		return "sbuapikey";
	}
	
	@Override
	public String getCommandUsage(ICommandSender sender) {
		return "/sbuapikey [key]";
	}
	
	@Override
	public void processCommand(ICommandSender sender, String[] args) throws CommandException {
		if(args.length == 1) {
			if(args[0].equals("reset")) {
				sender.addChatMessage(new ChatComponentText("Your API Key has been resettet!"));
			} else {
				System.out.println("Setting APIKey to " + args[0]);
				SkyBlockUtils.saves.apiKey = args[0];
			}
		} else {
			sender.addChatMessage(new ChatComponentText("Current API Key: " + SkyBlockUtils.saves.apiKey));
		}
	}
	
	@Override
	public boolean canCommandSenderUseCommand(ICommandSender sender) {
		return true;
	}
}
