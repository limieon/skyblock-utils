package com.gmasterhd.skyblockutils.commands;

import com.gmasterhd.skyblockutils.SkyBlockUtils;
import com.gmasterhd.skyblockutils.utils.APIRequests;
import com.gmasterhd.skyblockutils.utils.SkillXP;
import com.gmasterhd.skyblockutils.utils.Toolbox;
import com.google.gson.JsonObject;
import net.minecraft.client.util.JsonException;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

public class CommandSkills extends CommandBase {
	@Override
	public String getCommandName() {
		return "sbuskills";
	}
	
	@Override
	public String getCommandUsage(ICommandSender sender) {
		return "/sbuskills [username] [profilename]";
	}
	
	@Override
	public void processCommand(ICommandSender sender, String[] args) throws CommandException {
		if(sender instanceof EntityPlayer) {
			EntityPlayer player = (EntityPlayer) sender;
			if(args.length == 1) {
				System.out.println("Getting stats of " + args[0] + "...");
				System.out.println("Getting stats of " + player.getName());
				
				sender.addChatMessage(new ChatComponentText("Getting API Data of " + args[0] + "..."));
				JsonObject skyblockStats = Toolbox.getSkyBlockStats(args[0]);
				System.out.println(skyblockStats.toString());
				SkillXP alchemyXP = SkillXP.getLevelByXP(skyblockStats.get("experience_skill_alchemy").getAsDouble());
				SkillXP combatXP = SkillXP.getLevelByXP(skyblockStats.get("experience_skill_combat").getAsDouble());
				SkillXP enchantingXP = SkillXP.getLevelByXP(skyblockStats.get("experience_skill_enchanting").getAsDouble());
				SkillXP fishingXP = SkillXP.getLevelByXP(skyblockStats.get("experience_skill_fishing").getAsDouble());
				SkillXP foragingXP = SkillXP.getLevelByXP(skyblockStats.get("experience_skill_foraging").getAsDouble());
				SkillXP miningXP = SkillXP.getLevelByXP(skyblockStats.get("experience_skill_mining").getAsDouble());
				SkillXP tamingXP = SkillXP.getLevelByXP(skyblockStats.get("experience_skill_taming").getAsDouble());
				
				sender.addChatMessage(new ChatComponentText("Alchemy: Lvl. " + alchemyXP.level + ", " + (int)alchemyXP.progress + " / " + alchemyXP.nextLevel));
				sender.addChatMessage(new ChatComponentText("Combat: Lvl. " + combatXP.level + ", " + (int)combatXP.progress + " / " + combatXP.nextLevel));
				sender.addChatMessage(new ChatComponentText("Enchanting: Lvl. " + enchantingXP.level + ", " + (int)enchantingXP.progress + " / " + enchantingXP.nextLevel));
				sender.addChatMessage(new ChatComponentText("Fishing: Lvl. " + fishingXP.level + ", " + (int)fishingXP.progress + " / " + fishingXP.nextLevel));
				sender.addChatMessage(new ChatComponentText("Foraging: Lvl. " + foragingXP.level + ", " + (int)foragingXP.progress + " / " + foragingXP.nextLevel));
				sender.addChatMessage(new ChatComponentText("Mining: Lvl. " + miningXP.level + ", " + (int)miningXP.progress + " / " + miningXP.nextLevel));
				sender.addChatMessage(new ChatComponentText("Taming: Lvl. " + tamingXP.level + ", " + (int)tamingXP.progress + " / " + tamingXP.nextLevel));
				sender.addChatMessage(new ChatComponentText("Average Lvl.: " + (float)(alchemyXP.level + combatXP.level + enchantingXP.level + fishingXP.level + foragingXP.level + miningXP.level + tamingXP.level) / 7));
			} else if(args.length == 2) {
				System.out.println("Getting stats of " + args[0]);
				
				sender.addChatMessage(new ChatComponentText("Getting API Data of " + args[0] + "..."));
				JsonObject skyblockStats = Toolbox.getSkyBlockStats(args[0], args[1]);
				System.out.println(skyblockStats.toString());
				SkillXP alchemyXP = SkillXP.getLevelByXP(skyblockStats.get("experience_skill_alchemy").getAsDouble());
				SkillXP combatXP = SkillXP.getLevelByXP(skyblockStats.get("experience_skill_combat").getAsDouble());
				SkillXP enchantingXP = SkillXP.getLevelByXP(skyblockStats.get("experience_skill_enchanting").getAsDouble());
				SkillXP fishingXP = SkillXP.getLevelByXP(skyblockStats.get("experience_skill_fishing").getAsDouble());
				SkillXP foragingXP = SkillXP.getLevelByXP(skyblockStats.get("experience_skill_foraging").getAsDouble());
				SkillXP miningXP = SkillXP.getLevelByXP(skyblockStats.get("experience_skill_mining").getAsDouble());
				SkillXP tamingXP = SkillXP.getLevelByXP(skyblockStats.get("experience_skill_taming").getAsDouble());
				
				sender.addChatMessage(new ChatComponentText("Alchemy: Lvl. " + alchemyXP.level + ", " + (int)alchemyXP.progress + " / " + alchemyXP.nextLevel));
				sender.addChatMessage(new ChatComponentText("Combat: Lvl. " + combatXP.level + ", " + (int)combatXP.progress + " / " + combatXP.nextLevel));
				sender.addChatMessage(new ChatComponentText("Enchanting: Lvl. " + enchantingXP.level + ", " + (int)enchantingXP.progress + " / " + enchantingXP.nextLevel));
				sender.addChatMessage(new ChatComponentText("Fishing: Lvl. " + fishingXP.level + ", " + (int)fishingXP.progress + " / " + fishingXP.nextLevel));
				sender.addChatMessage(new ChatComponentText("Foraging: Lvl. " + foragingXP.level + ", " + (int)foragingXP.progress + " / " + foragingXP.nextLevel));
				sender.addChatMessage(new ChatComponentText("Mining: Lvl. " + miningXP.level + ", " + (int)miningXP.progress + " / " + miningXP.nextLevel));
				sender.addChatMessage(new ChatComponentText("Taming: Lvl. " + tamingXP.level + ", " + (int)tamingXP.progress + " / " + tamingXP.nextLevel));
				sender.addChatMessage(new ChatComponentText("Average Lvl.: " + (float)(alchemyXP.level + combatXP.level + enchantingXP.level + fishingXP.level + foragingXP.level + miningXP.level + tamingXP.level) / 7));
			} else {
				sender.addChatMessage(new ChatComponentText("Getting API Data of " + player.getName().toLowerCase() + "..."));
				JsonObject skyblockStats = Toolbox.getSkyBlockStats(player.getName().toLowerCase());
				System.out.println(skyblockStats.toString());
				SkillXP alchemyXP = SkillXP.getLevelByXP(skyblockStats.get("experience_skill_alchemy").getAsDouble());
				SkillXP combatXP = SkillXP.getLevelByXP(skyblockStats.get("experience_skill_combat").getAsDouble());
				SkillXP enchantingXP = SkillXP.getLevelByXP(skyblockStats.get("experience_skill_enchanting").getAsDouble());
				SkillXP fishingXP = SkillXP.getLevelByXP(skyblockStats.get("experience_skill_fishing").getAsDouble());
				SkillXP foragingXP = SkillXP.getLevelByXP(skyblockStats.get("experience_skill_foraging").getAsDouble());
				SkillXP miningXP = SkillXP.getLevelByXP(skyblockStats.get("experience_skill_mining").getAsDouble());
				SkillXP tamingXP = SkillXP.getLevelByXP(skyblockStats.get("experience_skill_taming").getAsDouble());
				
				sender.addChatMessage(new ChatComponentText("Alchemy: Lvl. " + alchemyXP.level + ", " + (int)alchemyXP.progress + " / " + alchemyXP.nextLevel));
				sender.addChatMessage(new ChatComponentText("Combat: Lvl. " + combatXP.level + ", " + (int)combatXP.progress + " / " + combatXP.nextLevel));
				sender.addChatMessage(new ChatComponentText("Enchanting: Lvl. " + enchantingXP.level + ", " + (int)enchantingXP.progress + " / " + enchantingXP.nextLevel));
				sender.addChatMessage(new ChatComponentText("Fishing: Lvl. " + fishingXP.level + ", " + (int)fishingXP.progress + " / " + fishingXP.nextLevel));
				sender.addChatMessage(new ChatComponentText("Foraging: Lvl. " + foragingXP.level + ", " + (int)foragingXP.progress + " / " + foragingXP.nextLevel));
				sender.addChatMessage(new ChatComponentText("Mining: Lvl. " + miningXP.level + ", " + (int)miningXP.progress + " / " + miningXP.nextLevel));
				sender.addChatMessage(new ChatComponentText("Taming: Lvl. " + tamingXP.level + ", " + (int)tamingXP.progress + " / " + tamingXP.nextLevel));
				sender.addChatMessage(new ChatComponentText("Average Lvl.: " + (float)(alchemyXP.level + combatXP.level + enchantingXP.level + fishingXP.level + foragingXP.level + miningXP.level + tamingXP.level) / 7));
			}
		} else {
			sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "This command doesn't work in the console!"));
		}
	}
	
	@Override
	public boolean canCommandSenderUseCommand(ICommandSender sender) {
		return true;
	}
}
