package com.gmasterhd.skyblockutils.enums;

public enum Rarity {
	COMMON,
	UNCOMMON,
	RARE,
	EPIC,
	LEGENDARY
}
